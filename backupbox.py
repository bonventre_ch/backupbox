import os
import click
import json
import socket
import subprocess
import re
import secrets
import string
import bcrypt
from pathlib import Path, PurePath
from shutil import which


# ToDo:
#   - handle non local restic repositories and let user choose
#   - refactor calls to restic into a function
#   - add doc string to all functions
#   - print version of dependencies and tell user to update if needed
#   - create commands to show info about backups and shares
#   - use restic with json output
#   - using the "init" command,
#           make sure the created files and directories have the right permissions and are in a valid location
#   - func to list dirs in "shares" with output for:
#       - is syncthing share : bool
#       - list of devices this share is shared with : str
#       - is included in backup : bool
#       - latest backup : date
#       -


def is_dependency_available(name) -> bool:
    """Check if a bin is installed and searchable with 'which' and returns True"""
    return which(name) is not None


def dependency_version(bin_name) -> str:
    command = ""
    if bin_name == "restic":
        command = f"restic version"
    elif bin_name == "restic-rest-server":  # ToDo: this is checking for an arch linux AUR package
        command = "restic-rest-server --version"
    elif bin_name == "syncthing":
        command = "syncthing --version"
    else:
        click.echo("ERROR: Unknown dependency.")
        click.Abort()

    result = subprocess.run(
        command.split(),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True
    )
    if result.returncode == 0:
        return result.stdout
    else:
        click.echo("ERROR: Dependency version lookup failed!")
        click.Abort()


def read_config() -> dict:
    """Read the currently saved config.json or create new one if non is available. Returns a dict."""
    config_file = Path("config.json")
    if config_file.is_file():
        with open(config_file, "r") as config:
            return json.load(config)
    else:
        click.echo("WARNING: Config file not found! Creating a new one.")
        new_config = {
            "name": "",
            "password_hash": "",
            "password_salt": "",
            "workspace_path": "",
            "ssh_key_path": "",
            "restic_repo_id": "",
            "restic_repo_pass_hash": "",
            "restic_repo_path": "",
            "syncthing_device_id": "",
            "syncthing_config_path": ""
        }
        write_config(new_config)
        return new_config


def write_config(new_config: dict):
    """Write the newly updated"""
    with open("config.json", "w") as f:
        json.dump(new_config, f, indent=4)


def check_password() -> tuple[str, bool]:
    """Read user input and compare it with the stored hash"""
    tries = 1
    current_config = read_config()
    password_hash = bytes(current_config.get("password_hash"), encoding="utf-8")
    password_salt = bytes(current_config.get("password_salt"), encoding="utf-8")
    while True:
        user_input = input("Password: ")
        combo_input_password = user_input.encode("utf-8") + password_salt
        hashed_input_password = bcrypt.hashpw(combo_input_password, password_salt)
        if hashed_input_password == password_hash:
            return user_input, True

        click.echo("Password is incorrect!")
        if tries < 3:
            tries += 1
            continue
        else:
            return "", False


def list_shares(
    current_config, 
    available_dirs: list, 
    multi_select=True
) -> list:
    """Returns a list of all directories within the BackupBox-Shares"""
    dirs_to_backup = []
    workspace_path = Path(current_config.get("workspace_path"))
    shares = workspace_path / "shares"
    selection_unsuccessful = True
    while selection_unsuccessful:
        click.echo("Choose from:")
        for directory in available_dirs:
            click.echo(f"{directory}")
        if multi_select:
            click.echo("Type a space ( ) separated list of the ones to back up")
        else:
            click.echo("Choose one from the list above")
        selection = input()
        for item in selection.split():  # split on " " (space)
            user_input = shares / item
            if PurePath(user_input).name not in available_dirs:
                click.echo(f"selection '{item}' is not a directory within '{shares}'")
                continue
            
            if not multi_select:
                dirs_to_backup[0] = item
                selection_unsuccessful = False
                break
            
            dirs_to_backup.append(item)
            selection_unsuccessful = False
            
    return dirs_to_backup


def is_human_readable(data, threshold=0.8) -> bool:
    """Takes a string and determines if it contains readable characters"""
    # ASCII printable characters
    readable_chars = [chr(i) for i in range(32, 127)]
    my_data = bytes(data, "utf-8")
    total_chars = len(my_data)
    readable_chars_count = sum(1 for c in my_data if chr(c) in readable_chars)

    return (readable_chars_count / total_chars) >= threshold


def share_dirs(current_config) -> list:
    workspace_path = Path(current_config.get("workspace_path"))
    shares = workspace_path / "shares"
    if not shares.is_dir():
        click.echo(f"BackupBox has not been initialized yet! run 'backupbox init' first.")
        raise click.Abort()
    items = shares.iterdir()
    # list of dirs in "shares" with just the last parent of a path (PurePath().name)
    return [PurePath(item).name for item in items if item.is_dir()]


def run_restic(command: str, password: str) -> subprocess.CompletedProcess:
    result = subprocess.run(
        command.split(),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        env=dict(os.environ, RESTIC_PASSWORD=password),
        text=True
    )
    if not result.returncode:
        return result
    click.echo(f"ERROR: run_restic failed with command: '{command}' !")
    return result


@click.group()
@click.option("--debug/--no-debug", default=True)  # ToDo: change this to "False" before prod
def cli(debug):
    """Synchronize and backup your files with BackupBox.\n
    BackupBox should always be run as non-root user!"""

    if debug:
        click.echo(f"Debug mode is on")
    
    if is_dependency_available("restic"):
        if debug:
            click.echo(f"{dependency_version('restic')}")
    else:
        click.echo("restic is not installed. Check here: https://restic.readthedocs.io/en/latest/")
        raise click.Abort()

    if is_dependency_available("syncthing"):
        if debug:
            click.echo(f"{dependency_version('syncthing')}")
    else:
        click.echo("syncthing is not installed. Check here: https://docs.syncthing.net/")
        raise click.Abort()

    if is_dependency_available("restic-rest-server"):  # ToDo: this is checking for an arch linux AUR package
        if debug:
            click.echo(f"{dependency_version('restic-rest-server')}")
    else:
        click.echo("restic-rest-server is not installed. Check here: https://github.com/restic/rest-server")
        raise click.Abort()


@cli.command()
def test_debug():
    """This function is only used to the cli with unittests. """
    click.echo("hello from test_debug")
    pass


@cli.command()
@click.option(
    "--name", "-n",
    type=str,
    required=True,
    default=socket.gethostname(),
    show_default=True,
    help="Name of this device",
    prompt="What should we call this device (only letters and numbers)"
)
@click.option(
    "--local", "-l",
    type=str,
    required=True,
    default="local",
    show_default=True,
    help="Physical location of the restic server",
    prompt="Should the instance be local (True) or remote (False)",
)
@click.option(
    "--path", "-d",
    type=str,
    required=True,
    default=".",
    show_default=True,
    help="Path in which BackupBox should be initialized ('.' means here).",
    prompt="Path to desired workspace"
)
def init(name, local, path):
    """Create a new BackupBox System, to use either locally or as a remote host."""
    # get the config
    current_config = read_config()

    clean_name = re.sub(r"[^a-zA-Z0-9]", "", name)
    if clean_name != name:
        if click.confirm(f"Name has been changed to: '{clean_name}', is this ok?", default=True):
            click.echo(f"Device name saved as: '{clean_name}'")
            current_config.update(name=clean_name)
        else:
            click.echo("Try again!")
            click.Abort()
    else:
        current_config.update(name=name)

    # ToDo
    if local == "local":
        pass
    elif local == "remote":
        pass
    else:
        # ToDo prompt the user to redo the selection
        pass

    # check if path is available and save it to the config
    if path == ".":
        path = Path.cwd()
    else:
        path = Path(path)
    path = Path(path / "backupbox-workspace")
    if Path(path).exists():
        click.echo(f"The directory at '{path}' already exists.")
        click.echo(f"Try one, in which the directory 'backupbox-workspace' does not already exist!")
        raise click.Abort()

    workspace = Path(path)
    workspace.mkdir()
    backup_dir = Path(path / "backup")
    backup_dir.mkdir()
    config_dir = Path(path / "config")
    config_dir.mkdir()
    shares_dir = Path(path / "shares")
    shares_dir.mkdir()

    current_config.update(workspace_path=str(path))

    # gen password for user
    alphabet = string.ascii_letters + string.digits
    password_length = 30
    password = ''.join(secrets.choice(alphabet) for i in range(password_length))
    salt = bcrypt.gensalt()
    combo_password = password.encode('utf-8') + salt  # ToDo: this seems weird to me, research
    hashed_password = bcrypt.hashpw(combo_password, salt)
    current_config.update(password_hash=hashed_password.decode("utf-8"))
    current_config.update(password_salt=salt.decode("utf-8"))
    write_config(current_config)
    click.echo(f"Your password is: '{password}', write it down now! ")
    click.echo(f"You and only you have this password and without it all your backups will be unusable.")

    user_input_password, valid_password = check_password()
    if not valid_password:
        click.echo("Password not valid!")
        raise click.Abort()

    command = f"restic init --repo {str(backup_dir)}"
    result = run_restic(command, user_input_password)
    try:
        restic_repo_id = re.search(r"restic repository (.+?) at", result.stdout).group(1)
        current_config.update(restic_repo_id=restic_repo_id)
    except AttributeError as e:
        click.echo("ERROR: Could not find restic repository id!")
        click.echo(e)
        raise click.Abort()

    click.echo("Successfully created a new BackupBox instance!")
    click.echo(f"Place your files in '{shares_dir}'")

    write_config(current_config)


@cli.command()
@click.option(
    "--all-dirs", "-a",
    type=bool,
    default=True,
    required=True,
    show_default=True,
    help="Select whether all available directories should be backed up",
    prompt="Should all available directories be backed up"
)
def backup(all_dirs):
    current_config = read_config()
    workspace_path = Path(current_config.get("workspace_path"))
    if not workspace_path:
        click.echo("ERROR: Run 'init' first!")
        raise click.Abort()

    dirs_to_backup = []
    if all_dirs:
        dirs_to_backup = share_dirs(current_config)
    else:
        dirs_to_backup = list_shares(current_config, share_dirs(current_config), True)

    # click.echo(f"about to back up the dirs: {dirs_to_backup}")
    for job in dirs_to_backup:
        backup_dir = workspace_path / "backups"
        share_dir = workspace_path / "shares"
        user_input_password, valid_password = check_password()
        if not valid_password:
            click.echo("Password not valid!")
            raise click.Abort()

        # ToDo: if remote rest-server change command

        command = f"restic -r {backup_dir} --verbose backup {share_dir / job}"
        result = run_restic(command, user_input_password)
        if not result.returncode:
            click.echo(f"Backup of '{job}' finished without problems")


@cli.command()
@click.option(
    "--dir", "-d",
    type=str,
    required=True,
    help="",
    prompt="Select a directory from which you'd like to restore files"
)
@click.option(
    "--location", "-l",
    type=str,
    required=True,
    help="",
    prompt="Select a directory you'd like to save the restored files to"
)
def restore():
    pass


@cli.command()
@click.option(
    "--directory", "-d",
    type=str,
    help="List human readable files from within snapshots",
)
def read_file(directory):
    """Disply a text file from within a backup to the CLI."""
    current_config = read_config()
    workspace_path = Path(current_config.get("workspace_path"))
    
    backup_dir = workspace_path / "backups"
    share_dir = workspace_path / "shares"

    # ToDo: check that a backup has been created

    selected_dir = list_shares(current_config, share_dirs(current_config), False)
    command = f"restic -r {backup_dir} ls -l latest {share_dir / selected_dir[0]}"
    user_input_password, valid_password = check_password()
    if not valid_password:
        click.echo("Password not valid!")
        raise click.Abort()

    result = subprocess.run(
        command.split(),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        env=dict(os.environ, RESTIC_PASSWORD=user_input_password),
        text=True
    )
    if result.returncode:
        click.echo("nothing found, maybe no backup?")
        raise click.Abort()
    try:
        files = re.findall(fr"/{selected_dir}/(.+?)\n", result.stdout, re.DOTALL)
    except AttributeError as e:
        click.echo("ERROR: Could not find file in snapshot!")
        click.echo(e)
        raise click.Abort()
    if not files:
        click.echo(files)
    for file in files:
        # ToDo: check that the file is in the selected_dir
        command = f"restic -r {backup_dir} dump latest {share_dir / selected_dir / file}"

        result = subprocess.run(
            command.split(),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            env=dict(os.environ, RESTIC_PASSWORD=user_input_password),
            text=True
        )
        if not result.returncode:
            if is_human_readable(result.stdout):
                click.echo(f"The content of '{file}' is:")
                click.echo(result.stdout)
                click.echo()


# used for debugging
# if __name__ == "__main__":
    # init()
