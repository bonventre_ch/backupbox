import re
import unittest
from unittest import mock
from click.testing import CliRunner
from backupbox import read_config, check_password, cli, init
from pathlib import Path


class TestCli(unittest.TestCase):

    def rm_tree(self, pth):
        pth = Path(pth)
        for child in pth.glob('*'):
            if child.is_file():
                child.unlink()
            else:
                self.rm_tree(child)
        pth.rmdir()

    def clean_workspace(self):
        self.rm_tree("./backupbox-workspace")

    def test_cli_print_usage(self):
        runner = CliRunner()
        result = runner.invoke(cli, ["--help"])
        assert result.exit_code == 0
        assert "Usage: cli [OPTIONS] COMMAND [ARGS]..." in result.output

    def test_cli_invalid_option(self):
        runner = CliRunner()
        result = runner.invoke(cli, ["--not-an-option"])
        assert result.exit_code == 2
        assert "Try 'cli --help' for help." in result.output
        assert "Error: No such option: --not-an-option" in result.output

    def test_cli_debug_on(self):
        runner = CliRunner()
        result = runner.invoke(cli, ["--debug", "test-debug"])
        assert result.exit_code == 0
        assert "Debug mode is on" in result.output
        assert "restic 0.1" in result.output  # update this if newer versions are being used
        assert "syncthing v1." in result.output
        assert "rest-server version rest-server 0.1" in result.output
        assert "hello from test_debug" in result.output

    def test_init_device_name(self,):
        if Path("./backupbox-workspace").is_dir():
            self.clean_workspace()
        runner = CliRunner()
        result = runner.invoke(init, input='\n'.join(["^co¼ç'mpu+ter_Na(me.", "True", ".", "y"]))
        assert "Name has been changed to: 'computerName', is this ok? [Y/n]: y" in result.stdout
        current_config = read_config()
        device_name = current_config.get("name")
        assert device_name == "computerName"

    def test_init_user_pw(self,):
        if Path("./backupbox-workspace").is_dir():
            self.clean_workspace()
        runner = CliRunner()
        result = runner.invoke(init, input='\n'.join(["computerName", "local", "."]))
        user_password = re.search(r"Your password is: '(.+?)', write it down now!", result.stdout).group(1)
        with unittest.mock.patch('builtins.input', return_value=user_password):
            pw_result = check_password()
            assert pw_result == (user_password, True)


if __name__ == "__main__":
    unittest.main()
