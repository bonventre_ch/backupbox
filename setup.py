from setuptools import setup

setup(
    name='backupbox',
    version='0.1.0',
    py_modules=['backupbox'],
    install_requires=[
        'Click',
        'bcrypt',
    ],
    entry_points={
        'console_scripts': [
            'backupbox = backupbox:cli',
        ],
    },
)
