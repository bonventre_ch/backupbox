# backupbox
leroy at bonventre dot ch


## Description
Automatically create incremental snapshots with [restic](https://restic.readthedocs.io/en/latest/)
Share directories with [SyncThing](https://docs.syncthing.net/)


## Installation
1. Clone the repository
2. Create and use a new virtual enviroment within the project directory
3. Make sure to use the python intepreter from with in the virtual enviroment
4. Use `setuptools` to make the script executable
5. Try the command `backupbox` to see if the help text is shown

```bash
git clone git@gitlab.com:bonventre_ch/backupbox.git
cd backupbox
python -m venv venv
. venv/bin/activate
pip install --editable .
backupbox
```

## Usage
See `backupbox --help`

### Usage flow
    1. Create a new workspace, e.g. "/home/leroy/backupbox-workspace"
    2. Initialize the BackupBox CLI which creates the directory structure and configuration files
    3. Move files in to BackupBox shares, e.g. "backupbox-workspace/share/pictures"
        3a. Share a directory with other devices/people
    4. Create a new backup repository for the above created shared directory
    5. Run the first backup job and define a backup schedule
    6. Restore files from a backup


### workspace structure

e.g. "/home/leroy/backupbox-workspace"

```
backupbox-workspace
    backupbox
    - backups
        - repositories created by restic
    - config
        - sub-directories with ids as names for each instance
    - shares
        - directories shared via syncthing
```

### config structure
```json
{
    "name": "",
    "password": "",
    "workspace_path": "",
    "ssh_key_path": "",
    "restic_repo_id": "",
    "restic_repo_pass_hash": "",
    "restic_repo_path": "",
    "syncthing_device_id": "",
    "syncthing_config_path": "",
    "password_hash": "",
    "password_salt": ""
}
```

## Support
This is mostly for myself. If you are going to use this, you are on your own.


## Roadmap

The BackupBox CLI features
- [ ] create workspace structure
- [ ] create new restic repository 
- [ ] backup to restic repository
- [ ] restore from restic repository
- [ ] create new SyncThing share
- [ ] provide framework for automatic backup


## License
GNU GENERAL PUBLIC LICENSE Version 3
See [LICENSE](LICENSE)
